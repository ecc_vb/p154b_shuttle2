﻿' Project:P154A_Shuttle1
' Auther:IE2A No.24, 村田直人
' Date: 2015年06月19日


Public Class Form1

    Dim ResetPoint As Point 'ラベルの初期座標
    Dim DownPoint As Point  'カーソルのロケーション(ラベル内)


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.ClientSize = New Size(400, 400 + PlayMenuItem.Height) 'フォームのサイズを設定

        'ラベルのロケーションを設定
        With ShuttleLabel1
            'ラベルの位置を初期化
            .Location = New Point((Panel1.ClientSize.Width - .Size.Width) \ 2, _
                                  (Panel1.ClientSize.Height - .Size.Height) \ 2)
            ResetPoint = ShuttleLabel1.Location 'ラベルの位置(初期化値)を格納
        End With

    End Sub

    Private Function ChangeIndex() As Integer
        Static n As Integer = 0 'counter
        Dim point As Integer    '返す値


        '戻り値の分岐
        If n = 0 Or n = 1 Then
            point = 0
        ElseIf n = 2 Or n = 3 Then
            point = 1
        ElseIf n = 4 Or n = 5 Then
            point = 2
        End If

        n += 1 'インクリメント

        'インクリメントor初期化分岐
        If n = 5 Then
            n = 0 '初期化
        End If

        Return point

    End Function

    'Keyが押された時
    Private Sub Form1_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown

        '押されたKeyを検査(シフト無し)
        With ShuttleLabel1

            Select Case e.KeyCode '押されたKeyの方向へ5px移動(シフト無し)
                Case Keys.Left
                    .Location = New Point(.Location.X - 5, .Location.Y) 'ラベルを←に移動
                Case Keys.Right
                    .Location = New Point(.Location.X + 5, .Location.Y) 'ラベルを→に移動
                Case Keys.Up
                    .Location = New Point(.Location.X, .Location.Y - 5) 'ラベルを↑に移動
                Case Keys.Down
                    .Location = New Point(.Location.X, .Location.Y + 5) 'ラベルを↓に移動
                Case Keys.Escape
                    If .Location.Equals(ResetPoint) = False Then
                        PlayResetMenuItem.PerformClick() 'リセットメニューのクリック処理呼び出し
                    End If
                Case Keys.None
                    Exit Sub 'プロシージャを終了
            End Select
        End With

        '押されたKeyを検査(シフトあり)
        With ShuttleLabel1
            Select Case e.KeyData '押されたKeyの方向へ5px移動(シフトあり)

                Case (Keys.Shift Or Keys.Left)
                    .Location = New Point(.Location.X - 5, .Location.Y) 'ラベルを←に移動    
                Case (Keys.Shift Or Keys.Right)
                    .Location = New Point(.Location.X + 5, .Location.Y) 'ラベルを→に移動
                Case (Keys.Shift Or Keys.Up)
                    .Location = New Point(.Location.X, .Location.Y - 5) 'ラベルを↑に移動
                Case (Keys.Shift Or Keys.Down)
                    .Location = New Point(.Location.X, .Location.Y + 5) 'ラベルを↓に移動
                Case Keys.Escape
                    If .Location.Equals(ResetPoint) = False Then
                        PlayResetMenuItem.PerformClick() 'リセットメニュー
                    End If
                Case Keys.None
                    Exit Sub 'プロシージャを終了
            End Select
        End With

        ShuttleLabel1.ImageIndex = ChangeIndex() 'Imageを設定

    End Sub

    Private Sub ShuttleLabel1_LocationChanged _
        (ByVal sender As Object, ByVal e As EventArgs) _
        Handles Me.Shown, ShuttleLabel1.LocationChanged

        If ShuttleLabel1.Location.Equals(ResetPoint) Then
            PlayResetMenuItem.Enabled = False '無効化
        Else
            PlayResetMenuItem.Enabled = True '有効化
        End If

    End Sub

    Private Sub PlayResetMenuItem_Click(sender As Object, e As EventArgs) Handles PlayResetMenuItem.Click

        If MessageBox.Show("シャトルを画面の中央に移動します。", Me.Text, _
                           MessageBoxButtons.OKCancel, MessageBoxIcon.Information) _
                            = Windows.Forms.DialogResult.OK Then

            ShuttleLabel1.Location = ResetPoint 'ラベルの位置を初期化
        Else
            Exit Sub 'プロシージャを終了

        End If

    End Sub

    Private Sub PlayExitMenuItem_Click(sender As Object, e As EventArgs) Handles PlayExitMenuItem.Click
        Me.Close() 'フォームを閉じる
    End Sub

    'キーを離した
    Private Sub Form1_KeyUp(sender As Object, e As KeyEventArgs) Handles MyBase.KeyUp
        ShuttleLabel1.ImageIndex = 0 'ラベルをもとに戻す
    End Sub

    'Click on the lable(マウスをクリック)
    Private Sub ShuttleLabel1_MouseDown(sender As Object, e As MouseEventArgs) Handles ShuttleLabel1.MouseDown

        If e.Button <> MouseButtons.Left Then
            ShuttleLabel1.Capture = False 'キャプチャ解除
            Exit Sub 'プロシージャを抜ける
        End If

        DownPoint = e.Location 'クリックした
        ShuttleLabel1.Cursor = Cursors.SizeAll 'カーソルを変更

    End Sub

    'Move on the Mouse
    Private Sub ShuttleLabel1_MouseMove(sender As Object, e As MouseEventArgs) Handles ShuttleLabel1.MouseMove

        If ShuttleLabel1.Capture = False Then
            Exit Sub 'プロシージャを終了
        End If

        Dim movePoint As Point = e.Location 'マウスポインタの位置を格納
        movePoint.Offset(-DownPoint.X, -DownPoint.Y) '画像の位置調整

        '移動距離を計算して処理を分岐
        If Math.Sqrt((movePoint.X ^ 2) + (movePoint.Y ^ 2)) < 5 Then
            Exit Sub 'プロシージャを抜ける
        End If

        With ShuttleLabel1
            movePoint.Offset(.Location) 'ポイントをクライアント座標に変更
            .Location = movePoint 'ラベルの表示を移動
            .ImageIndex = ChangeIndex() 'イメージの切り替え
        End With

    End Sub

    Private Sub ShuttleLabel1_MouseUp(sender As Object, e As MouseEventArgs) Handles ShuttleLabel1.MouseUp

        'キャプチャしてるか判断
        If ShuttleLabel1.Capture = False Then
            Exit Sub 'プロシージャを抜ける
        End If

        With ShuttleLabel1
            .ImageIndex = 0 'ラベルを初期化
            .Cursor = Cursors.Default 'カーソルをデフォルトに戻す
        End With

    End Sub
End Class
